FROM php:8.4-apache
COPY Dockerfile /var/www/html
COPY html /var/www/html
RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable mysqli
