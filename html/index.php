<?php
// Inicia el buffer y la sesión
ob_start();
session_start();

// Si el usuario ya está logueado, redirige a bienvenido.php
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: bienvenido.php");
    exit;
}

// Incluye archivos de configuración y cabecera
require_once "config.php";
require_once "header.php";

// Inicializa variables vacías y errores
$username = $password = "";
$username_err = $password_err = $login_err = "";

// Procesa el formulario al enviarlo
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validación del campo usuario
    if(empty(trim($_POST["username"]))){
        $username_err = "Por favor, ingrese usuario.";
    } else{
        $username = trim($_POST["username"]);
    }

    // Validación del campo contraseña
    if(empty(trim($_POST["password"]))){
        $password_err = "Por favor, ingrese contraseña.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Si no hay errores, se prepara la consulta
    if(empty($username_err) && empty($password_err)){
        $sql = "SELECT id, username, password FROM users WHERE username = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = $username;

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                // Verifica si existe el usuario
                if(mysqli_stmt_num_rows($stmt) == 1){
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Almacena los datos en sesión
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;
                            header("location: bienvenido.php");
                        } else{
                            $login_err = "Datos incorrectos.";
                        }
                    }
                } else{
                    $login_err = "Datos incorrectos.";
                }
            } else{
                echo "Perdón! Ocurrió un error, intente nuevamente.";
            }
            mysqli_stmt_close($stmt);
        }
    }
    mysqli_close($link);
}
?>

<!-- Contenedor principal -->
<div class="container-fluid vh-100">
  <div class="row h-100">
    <!-- Columna con imagen de fondo (visible en md en adelante) -->
    <div class="col-md-6 d-none d-md-flex bg-image" style="background: url('https://cdn.dooklik.com/uploadImages/ProductImages/Product-P-19-637487393697431745.jpg') center center/cover no-repeat;"></div>

    <!-- Columna del formulario de ingreso -->
    <div class="col-md-6 d-flex align-items-center bg-light">
      <div class="w-100 py-5">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-6">
              <div class="card shadow-sm border-0">
                <div class="card-body p-4">
                  <h2 class="card-title text-center mb-4">Panel de Control</h2>
                  <?php
                  if(!empty($login_err)){
                      echo '<div class="alert alert-danger">' . $login_err . '</div>';
                  }
                  ?>
                  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                      <label for="username">Usuario</label>
                      <input type="text" name="username" id="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo htmlspecialchars($username); ?>">
                      <span class="invalid-feedback"><?php echo $username_err; ?></span>
                    </div>
                    <div class="form-group">
                      <label for="password">Clave</label>
                      <input type="password" name="password" id="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                      <span class="invalid-feedback"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group mt-4">
                      <input type="submit" class="btn btn-primary btn-lg btn-block" value="Ingresar">
                    </div>
                  </form>
                  <p class="text-center mt-3">¿No tenés cuenta? <a href="registrar.php">¡Registrate!</a></p>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.w-100 -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->

<?php
include "footer.php";
ob_end_flush();
?>
