<?php
ob_start();
session_start();

// Verificar si el usuario está logueado
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: index.php");
    exit;
}

require_once "header.php";
require_once "menu.php";

// Ruta al Dockerfile
$dockerfilePath = '/var/www/html/Dockerfile';

// Verificar si el archivo existe y tiene permisos de lectura/escritura
if (!file_exists($dockerfilePath)) {
    $errorMessage = "Error: El archivo Dockerfile no existe.";
} elseif (!is_readable($dockerfilePath) || !is_writable($dockerfilePath)) {
    $errorMessage = "Error: No tienes permisos suficientes para modificar el Dockerfile.";
}

// Leer el contenido del Dockerfile
$dockerfileContent = is_readable($dockerfilePath) ? file_get_contents($dockerfilePath) : "";

// Lista de versiones PHP válidas
$availableVersions = ['7.2', '7.4', '8.0', '8.1', '8.2', '8.3', '8.4'];

// Función para validar la versión de PHP
function sanitizePhpVersion($version, $availableVersions) {
    return in_array($version, $availableVersions) ? $version : false;
}

// Si se envió el formulario para cambiar la versión de PHP
if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['php_version'])) {
    $phpVersion = sanitizePhpVersion($_POST['php_version'], $availableVersions);

    if ($phpVersion === false) {
        $errorMessage = "Versión de PHP no válida.";
    } else {
        // Reemplazar solo la versión en la línea FROM php:X.X-apache
        $newDockerfileContent = preg_replace('/FROM php:\d+\.\d+-apache/', "FROM php:$phpVersion-apache", $dockerfileContent);

        // Verificar si hubo cambios antes de sobrescribir el archivo
        if ($newDockerfileContent !== $dockerfileContent) {
            if (file_put_contents($dockerfilePath, $newDockerfileContent) !== false) {
                $successMessage = "Dockerfile actualizado con la versión PHP: $phpVersion";
                $dockerfileContent = $newDockerfileContent;
            } else {
                $errorMessage = "Error al escribir en el Dockerfile. Verifica permisos.";
            }
        } else {
            $warningMessage = "No se realizaron cambios en el Dockerfile.";
        }
    }
}

require_once "sidebar.php";
?>

<br><br>
<div class="container">
    <h2>Actualizar versión de PHP</h2>

    <form method="post" action="servidor.php">
        <div class="form-group">
            <label for="php_version">Selecciona la versión de PHP:</label>
            <select id="php_version" name="php_version" class="form-control">
                <?php
                foreach ($availableVersions as $version) {
                    $selected = (strpos($dockerfileContent, "FROM php:$version-apache") !== false) ? 'selected' : '';
                    echo "<option value=\"$version\" $selected>PHP $version</option>";
                }
                ?>
            </select>
        </div>

        <button type="submit" class="btn btn-primary mt-2">Actualizar PHP</button>
    </form>

    <br><br>

    <!-- Mensajes de éxito, advertencia o error -->
    <?php if (isset($successMessage)): ?>
        <div class="alert alert-success"><?php echo htmlspecialchars($successMessage); ?></div>
    <?php elseif (isset($warningMessage)): ?>
        <div class="alert alert-warning"><?php echo htmlspecialchars($warningMessage); ?></div>
    <?php elseif (isset($errorMessage)): ?>
        <div class="alert alert-danger"><?php echo htmlspecialchars($errorMessage); ?></div>
    <?php endif; ?>
</div>

<?php
include "footer.php";
?>
