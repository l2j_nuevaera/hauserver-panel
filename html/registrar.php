<?php
require_once "config.php";
require_once "header.php";

$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validación del campo Email (usuario)
    if(empty(trim($_POST["username"]))){
        $username_err = "Por favor, ingrese usuario.";
    } elseif(!preg_match('/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', trim($_POST["username"]))){
        $username_err = "Recuerde ingresar un email válido.";
    } else{
        $sql = "SELECT id FROM users WHERE username = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = trim($_POST["username"]);

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "Este nombre de usuario ya está en uso.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Por favor, inténtelo de nuevo más tarde.";
            }
            mysqli_stmt_close($stmt);
        }
    }

    // Validación de la contraseña
    if(empty(trim($_POST["password"]))){
        $password_err = "Por favor, introduzca una contraseña.";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "La contraseña debe tener al menos 6 caracteres.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validación de la confirmación de contraseña
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Por favor, confirme la contraseña.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Las contraseñas no coinciden.";
        }
    }

    // Si no hay errores, inserta el usuario en la base de datos
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);

            if(mysqli_stmt_execute($stmt)){
                header("location: index.php");
            } else{
                echo "Por favor, inténtelo de nuevo más tarde.";
            }
            mysqli_stmt_close($stmt);
        }
    }
    mysqli_close($link);
}
?>

<!-- Diseño mejorado para la página de registro -->
<div class="container-fluid vh-100">
  <div class="row h-100">
    <!-- Columna con imagen de fondo (se muestra en dispositivos md+ ) -->
    <div class="col-md-6 d-none d-md-flex bg-image" style="background: url('https://cdn.dooklik.com/uploadImages/ProductImages/Product-P-19-637487393697431745.jpg') center center/cover no-repeat;"></div>
    <!-- Columna del formulario de registro -->
    <div class="col-md-6 d-flex align-items-center bg-light">
      <div class="w-100 py-5">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-6">
              <div class="card shadow-sm border-0">
                <div class="card-body p-4">
                  <h2 class="card-title text-center mb-4">Registrarse</h2>
                  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo htmlspecialchars($username); ?>">
                      <span class="invalid-feedback"><?php echo $username_err; ?></span>
                    </div>
                    <div class="form-group">
                      <label>Clave</label>
                      <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo htmlspecialchars($password); ?>">
                      <span class="invalid-feedback"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group">
                      <label>Confirmar Clave</label>
                      <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo htmlspecialchars($confirm_password); ?>">
                      <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
                    </div>
                    <div class="form-group mt-4">
                      <input type="submit" class="btn btn-primary btn-lg btn-block" value="Registrar">
                      <input type="reset" class="btn btn-secondary btn-lg btn-block" value="Limpiar Campos">
                    </div>
                    <p class="text-center mt-3">¿Ya tenés cuenta? <a href="index.php">¡Ingresá!</a></p>
                  </form>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.w-100 -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->

<?php
include "footer.php";
?>
