<?php
// Verifica si la sesión no ha sido iniciada antes de ejecutarla
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Obtener datos de la sesión
$usuario = isset($_SESSION["username"]) ? $_SESSION["username"] : "Invitado";
$pais = isset($_SESSION["pais"]) ? $_SESSION["pais"] : "AR"; // Pais por defecto en caso de no haber selección
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button id="sidebarToggle" class="btn-circle mr-2" onclick="toggleNav()">
    <i class="fas fa-bars"></i>
  </button>
  <a class="navbar-brand" href="#">Panel de Control</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-question-circle"></i> Ayuda</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown">
          <?php echo htmlspecialchars($usuario, ENT_QUOTES, 'UTF-8'); ?>
          <span class="ml-2">
            <img src="https://flagcdn.com/w20/<?php echo strtolower($pais); ?>.png" alt="Flag" class="rounded-circle" width="30" height="30">
          </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="perfil.php"><i class="fas fa-user"></i> Mi Perfil</a>
	  <a class="dropdown-item" href="reiniciar-clave.php"><i class="fas fa-key"></i> Cambiar Clave</a>
          <a class="dropdown-item text-danger" href="logout.php"><i class="fas fa-sign-out-alt"></i> Salir</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
