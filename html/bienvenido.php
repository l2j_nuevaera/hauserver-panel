<?php
ob_start();
session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

require_once "header.php";
require_once "menu.php";
require_once "sidebar.php";
?>

<?php
include "footer.php";
?>
