<?php
session_start();
require_once "config.php";
require_once "menu.php"; // Asegúrate de que 'menu.php' esté incluido antes de 'sidebar.php'

// Verificar si el usuario está autenticado
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: index.php");
    exit;
}

// Variables para almacenar mensajes
$mensaje = "";

// Obtener datos del usuario
$id_usuario = $_SESSION["id"];
$usuario = []; // Inicializamos como array vacío

// Intentar obtener los datos del usuario
$sql = "SELECT nombre, apellido, dni, pais, domicilio, calle, ciudad, provincia, cp FROM users WHERE id = ?";
if ($stmt = $link->prepare($sql)) { // Usamos $link en vez de $mysqli
    $stmt->bind_param("i", $id_usuario);
    $stmt->execute();
    $stmt->store_result();  // Almacena los resultados antes de hacer fetch_assoc()

    // Asociar las variables de los resultados
    $stmt->bind_result($nombre, $apellido, $dni, $pais, $domicilio, $calle, $ciudad, $provincia, $cp);

    if ($stmt->fetch()) {
        // Asignamos los resultados en el array usuario
        $usuario = [
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dni' => $dni,
            'pais' => $pais,
            'domicilio' => $domicilio,
            'calle' => $calle,
            'ciudad' => $ciudad,
            'provincia' => $provincia,
            'cp' => $cp
        ];
    }
    $stmt->close(); // Cerrar la declaración después de usarla
} else {
    $mensaje = "Error al obtener los datos del usuario.";
}

// Procesar formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $dni = $_POST["dni"];
    $pais = $_POST["pais"];
    $domicilio = $_POST["domicilio"];
    $calle = $_POST["calle"];
    $ciudad = $_POST["ciudad"];
    $provincia = $_POST["provincia"];
    $cp = $_POST["cp"];

    // Guardar el país en la sesión para mostrar la bandera
    $_SESSION["pais"] = $pais;

    // Preparar la consulta de actualización
    $sql = "UPDATE users SET nombre=?, apellido=?, dni=?, pais=?, domicilio=?, calle=?, ciudad=?, provincia=?, cp=? WHERE id=?";
    if ($stmt = $link->prepare($sql)) { // Usamos $link en vez de $mysqli
        // Vinculamos los parámetros y ejecutamos la consulta
        $stmt->bind_param("sssssssssi", $nombre, $apellido, $dni, $pais, $domicilio, $calle, $ciudad, $provincia, $cp, $id_usuario);
        if ($stmt->execute()) {
            $mensaje = "Datos actualizados correctamente.";
        } else {
            $mensaje = "Error al actualizar los datos.";
        }
        $stmt->close(); // Cerrar la declaración después de ejecutar
    } else {
        $mensaje = "Error al preparar la consulta de actualización.";
    }
}

// Re-obtenemos los datos actualizados del usuario después de la actualización (si es necesario)
$sql = "SELECT nombre, apellido, dni, pais, domicilio, calle, ciudad, provincia, cp FROM users WHERE id = ?";
if ($stmt = $link->prepare($sql)) {
    $stmt->bind_param("i", $id_usuario);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($nombre, $apellido, $dni, $pais, $domicilio, $calle, $ciudad, $provincia, $cp);
    if ($stmt->fetch()) {
        // Asignamos los resultados en el array usuario
        $usuario = [
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dni' => $dni,
            'pais' => $pais,
            'domicilio' => $domicilio,
            'calle' => $calle,
            'ciudad' => $ciudad,
            'provincia' => $provincia,
            'cp' => $cp
        ];
    }
    $stmt->close(); // Cerrar la declaración después de obtener los resultados
}

$link->close(); // Cerramos la conexión a la base de datos

require_once "sidebar.php"; // Asegúrate de incluir 'sidebar.php' después de 'menu.php'
require_once "header.php";
?>

<div class="container mt-4">
    <h2>Actualizar Perfil</h2>
    <?php if (!empty($mensaje)) echo '<div class="alert alert-info">' . htmlspecialchars($mensaje) . '</div>'; ?>
    <form method="post">
        <div class="row">
            <div class="col-md-6">
                <label>Nombre</label>
                <input type="text" name="nombre" class="form-control" value="<?php echo htmlspecialchars($usuario['nombre'] ?? ''); ?>" required>
            </div>
            <div class="col-md-6">
                <label>Apellido</label>
                <input type="text" name="apellido" class="form-control" value="<?php echo htmlspecialchars($usuario['apellido'] ?? ''); ?>" required>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-6">
                <label>DNI</label>
                <input type="text" name="dni" class="form-control" value="<?php echo htmlspecialchars($usuario['dni'] ?? ''); ?>" required>
            </div>
            <div class="col-md-6">
                <label>País</label>
                <select name="pais" class="form-control" required>
                    <?php
                    // Lista de países y códigos de bandera
			$paises = [
    'AD' => 'Andorra',
    'AE' => 'Emiratos Árabes Unidos',
    'AF' => 'Afganistán',
    'AG' => 'Antigua y Barbuda',
    'AI' => 'Anguila',
    'AL' => 'Albania',
    'AM' => 'Armenia',
    'AO' => 'Angola',
    'AR' => 'Argentina',
    'AS' => 'Samoa Americana',
    'AT' => 'Austria',
    'AU' => 'Australia',
    'AW' => 'Aruba',
    'AX' => 'Islas Åland',
    'AZ' => 'Azerbaiyán',
    'BA' => 'Bosnia y Herzegovina',
    'BB' => 'Barbados',
    'BD' => 'Bangladesh',
    'BE' => 'Bélgica',
    'BF' => 'Burkina Faso',
    'BG' => 'Bulgaria',
    'BH' => 'Baréin',
    'BI' => 'Burundi',
    'BJ' => 'Benín',
    'BL' => 'San Bartolomé',
    'BM' => 'Bermudas',
    'BN' => 'Brunéi',
    'BO' => 'Bolivia',
    'BQ' => 'Bonaire, San Eustaquio y Saba',
    'BR' => 'Brasil',
    'BS' => 'Bahamas',
    'BT' => 'Bután',
    'BV' => 'Isla Bouvet',
    'BW' => 'Botsuana',
    'BY' => 'Bielorrusia',
    'BZ' => 'Belice',
    'CA' => 'Canadá',
    'CC' => 'Islas Cocos (Keeling)',
    'CD' => 'República Democrática del Congo',
    'CF' => 'República Centroafricana',
    'CG' => 'Congo',
    'CH' => 'Suiza',
    'CI' => 'Costa de Marfil',
    'CK' => 'Islas Cook',
    'CL' => 'Chile',
    'CM' => 'Camerún',
    'CN' => 'China',
    'CO' => 'Colombia',
    'CR' => 'Costa Rica',
    'CU' => 'Cuba',
    'CV' => 'Cabo Verde',
    'CW' => 'Curazao',
    'CX' => 'Isla Christmas',
    'CY' => 'Chipre',
    'CZ' => 'República Checa',
    'DE' => 'Alemania',
    'DJ' => 'Yibuti',
    'DK' => 'Dinamarca',
    'DM' => 'Dominica',
    'DO' => 'República Dominicana',
    'DZ' => 'Argelia',
    'EC' => 'Ecuador',
    'EE' => 'Estonia',
    'EG' => 'Egipto',
    'EH' => 'Sahara Occidental',
    'ER' => 'Eritrea',
    'ES' => 'España',
    'ET' => 'Etiopía',
    'FI' => 'Finlandia',
    'FJ' => 'Fiyi',
    'FM' => 'Micronesia',
    'FO' => 'Islas Feroe',
    'FR' => 'Francia',
    'GA' => 'Gabón',
    'GB' => 'Reino Unido',
    'GD' => 'Granada',
    'GE' => 'Georgia',
    'GF' => 'Guayana Francesa',
    'GG' => 'Guernesey',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GL' => 'Groenlandia',
    'GM' => 'Gambia',
    'GN' => 'Guinea',
    'GP' => 'Guadalupe',
    'GQ' => 'Guinea Ecuatorial',
    'GR' => 'Grecia',
    'GT' => 'Guatemala',
    'GU' => 'Guam',
    'GW' => 'Guinea-Bisáu',
    'GY' => 'Guyana',
    'HK' => 'Hong Kong',
    'HM' => 'Islas Heard y McDonald',
    'HN' => 'Honduras',
    'HR' => 'Croacia',
    'HT' => 'Haití',
    'HU' => 'Hungría',
    'ID' => 'Indonesia',
    'IE' => 'Irlanda',
    'IL' => 'Israel',
    'IM' => 'Isla de Man',
    'IN' => 'India',
    'IO' => 'Territorio Británico del Océano Índico',
    'IQ' => 'Irak',
    'IR' => 'Irán',
    'IS' => 'Islandia',
    'IT' => 'Italia',
    'JE' => 'Jersey',
    'JM' => 'Jamaica',
    'JO' => 'Jordania',
    'JP' => 'Japón',
    'KE' => 'Kenia',
    'KG' => 'Kirguistán',
    'KH' => 'Camboya',
    'KI' => 'Kiribati',
    'KM' => 'Comoras',
    'KN' => 'San Cristóbal y Nieves',
    'KP' => 'Corea del Norte',
    'KR' => 'Corea del Sur',
    'KW' => 'Kuwait',
    'KY' => 'Islas Caimán',
    'KZ' => 'Kazajistán',
    'LA' => 'Laos',
    'LB' => 'Líbano',
    'LC' => 'Santa Lucía',
    'LI' => 'Liechtenstein',
    'LK' => 'Sri Lanka',
    'LR' => 'Liberia',
    'LS' => 'Lesoto',
    'LT' => 'Lituania',
    'LU' => 'Luxemburgo',
    'LV' => 'Letonia',
    'LY' => 'Libia',
    'MA' => 'Marruecos',
    'MC' => 'Mónaco',
    'MD' => 'Moldavia',
    'ME' => 'Montenegro',
    'MF' => 'San Martín (Francia)',
    'MG' => 'Madagascar',
    'MH' => 'Islas Marshall',
    'MK' => 'Macedonia del Norte',
    'ML' => 'Malí',
    'MM' => 'Birmania',
    'MN' => 'Mongolia',
    'MO' => 'Macao',
    'MP' => 'Islas Marianas del Norte',
    'MQ' => 'Martinica',
    'MR' => 'Mauritania',
    'MS' => 'Montserrat',
    'MT' => 'Malta',
    'MU' => 'Mauricio',
    'MV' => 'Maldivas',
    'MW' => 'Malawi',
    'MX' => 'México',
    'MY' => 'Malasia',
    'MZ' => 'Mozambique',
    'NA' => 'Namibia',
    'NC' => 'Nueva Caledonia',
    'NE' => 'Níger',
    'NF' => 'Isla Norfolk',
    'NG' => 'Nigeria',
    'NI' => 'Nicaragua',
    'NL' => 'Países Bajos',
    'NO' => 'Noruega',
    'NP' => 'Nepal',
    'NR' => 'Nauru',
    'NU' => 'Niue',
    'NZ' => 'Nueva Zelanda',
    'OM' => 'Omán',
    'PA' => 'Panamá',
    'PE' => 'Perú',
    'PF' => 'Polinesia Francesa',
    'PG' => 'Papúa Nueva Guinea',
    'PH' => 'Filipinas',
    'PK' => 'Pakistán',
    'PL' => 'Polonia',
    'PM' => 'San Pedro y Miquelón',
    'PN' => 'Islas Pitcairn',
    'PR' => 'Puerto Rico',
    'PT' => 'Portugal',
    'PW' => 'Palaos',
    'PY' => 'Paraguay',
    'QA' => 'Catar',
    'RE' => 'Reunión',
    'RO' => 'Rumanía',
    'RS' => 'Serbia',
    'RU' => 'Rusia',
    'RW' => 'Ruanda',
    'SA' => 'Arabia Saudita',
    'SB' => 'Islas Salomón',
    'SC' => 'Seychelles',
    'SD' => 'Sudán',
    'SE' => 'Suecia',
    'SG' => 'Singapur',
    'SH' => 'Santa Elena',
    'SI' => 'Eslovenia',
    'SJ' => 'Svalbard y Jan Mayen',
    'SK' => 'Eslovaquia',
    'SL' => 'Sierra Leona',
    'SM' => 'San Marino',
    'SN' => 'Senegal',
    'SO' => 'Somalia',
    'SR' => 'Surinam',
    'SS' => 'Sudán del Sur',
    'ST' => 'Santo Tomé y Príncipe',
    'SV' => 'El Salvador',
    'SX' => 'San Martín (Sint Maarten)',
    'SY' => 'Siria',
    'SZ' => 'Suazilandia',
    'TC' => 'Islas Turcas y Caicos',
    'TD' => 'Chad',
    'TF' => 'Tierras Australes Francesas',
    'TG' => 'Togo',
    'TH' => 'Tailandia',
    'TJ' => 'Tayikistán',
    'TK' => 'Tokelau',
    'TL' => 'Timor Oriental',
    'TM' => 'Turkmenistán',
    'TN' => 'Túnez',
    'TO' => 'Tonga',
    'TR' => 'Turquía',
    'TT' => 'Trinidad y Tobago',
    'TV' => 'Tuvalu',
    'TZ' => 'Tanzania',
    'UA' => 'Ucrania',
    'UG' => 'Uganda',
    'UM' => 'Islas Ultramarinas Menores de EE. UU.',
    'US' => 'Estados Unidos',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistán',
    'VA' => 'Ciudad del Vaticano',
    'VC' => 'San Vicente y las Granadinas',
    'VE' => 'Venezuela',
    'VG' => 'Islas Vírgenes Británicas',
    'VI' => 'Islas Vírgenes de los EE. UU.',
    'VN' => 'Vietnam',
    'VU' => 'Vanuatu',
    'WF' => 'Wallis y Futuna',
    'WS' => 'Samoa',
    'YE' => 'Yemen',
    'YT' => 'Mayotte',
    'ZA' => 'Sudáfrica',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabue'
];

                    // Obtener el país seleccionado (por defecto, de la sesión o base de datos)
                    $paisSeleccionado = isset($_SESSION["pais"]) ? $_SESSION["pais"] : $usuario['pais']; // Usar el país de la base de datos si no está en la sesión

                    // Mostrar el select con las banderas y nombres de países
                    foreach ($paises as $codigo => $nombre) {
                        $selected = ($paisSeleccionado == $codigo) ? 'selected' : '';
                        echo "<option value=\"$codigo\" $selected>$nombre</option>";
                    }
                    ?>
                </select>
            </div>
        </div>

        <!-- Mostrar la bandera del país seleccionado -->
        <div class="mt-3">
            <label>Bandera:</label><br>
            <img src="https://flagcdn.com/w20/<?php echo strtolower($paisSeleccionado); ?>.png" alt="Bandera" class="rounded-circle" width="30" height="30">
        </div>

        <div class="row mt-2">
            <div class="col-md-6">
                <label>Domicilio</label>
                <input type="text" name="domicilio" class="form-control" value="<?php echo htmlspecialchars($usuario['domicilio'] ?? ''); ?>">
            </div>
            <div class="col-md-6">
                <label>Calle</label>
                <input type="text" name="calle" class="form-control" value="<?php echo htmlspecialchars($usuario['calle'] ?? ''); ?>">
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-4">
                <label>Ciudad</label>
                <input type="text" name="ciudad" class="form-control" value="<?php echo htmlspecialchars($usuario['ciudad'] ?? ''); ?>">
            </div>
            <div class="col-md-4">
                <label>Provincia</label>
                <input type="text" name="provincia" class="form-control" value="<?php echo htmlspecialchars($usuario['provincia'] ?? ''); ?>">
            </div>
            <div class="col-md-4">
                <label>Código Postal</label>
                <input type="text" name="cp" class="form-control" value="<?php echo htmlspecialchars($usuario['cp'] ?? ''); ?>">
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-3">Actualizar</button>
    </form>
</div>

<?php require_once "footer.php"; ?>
