<div id="mySidenav" class="sidenav">
  <div class="sidebar-header">
  </div>
  <a href="bienvenido.php"><span class="fas fa-home"></span> Inicio</a>

  <!-- Accesos con submenú desplegable -->
  <a href="#" class="list-group-item list-group-item-action" data-bs-toggle="collapse" data-bs-target="#accesosSubmenu" aria-expanded="false" aria-controls="accesosSubmenu">
    <span class="fas fa-user"></span> Accesos
  </a>
  <div id="accesosSubmenu" class="collapse pl-4">
    <a href="#" class="list-group-item list-group-item-action">Administradores</a>
    <a href="#" class="list-group-item list-group-item-action">Invitaciones</a>
  </div>

  <!-- Estadísticas con submenú -->
  <a href="#" class="list-group-item list-group-item-action" data-bs-toggle="collapse" data-bs-target="#estadisticasSubmenu" aria-expanded="false" aria-controls="estadisticasSubmenu">
    <span class="fas fa-chart-bar"></span> Estadísticas
  </a>
  <div id="estadisticasSubmenu" class="collapse pl-4">
    <a href="#" class="list-group-item list-group-item-action">Estadísticas Web</a>
    <a href="#" class="list-group-item list-group-item-action">Estadísticas Correo</a>
  </div>

  <!-- Dominios con submenú -->
  <a href="#" class="list-group-item list-group-item-action" data-bs-toggle="collapse" data-bs-target="#dominiosSubmenu" aria-expanded="false" aria-controls="dominiosSubmenu">
    <span class="fas fa-cloud"></span> Dominios
  </a>
  <div id="dominiosSubmenu" class="collapse pl-4">
    <a href="#" class="list-group-item list-group-item-action">Instalador de Aplicaciones</a>
  </div>

  <!-- Bases de Datos con submenú -->
  <a href="#" class="list-group-item list-group-item-action" data-bs-toggle="collapse" data-bs-target="#basesDeDatosSubmenu" aria-expanded="false" aria-controls="basesDeDatosSubmenu">
    <span class="fas fa-database"></span> Bases de Datos
  </a>
  <div id="basesDeDatosSubmenu" class="collapse pl-4">
    <a href="#" class="list-group-item list-group-item-action">Bases de Datos</a>
    <a href="#" class="list-group-item list-group-item-action">Usuario</a>
  </div>

  <!-- Email con submenú -->
  <a href="#" class="list-group-item list-group-item-action" data-bs-toggle="collapse" data-bs-target="#emailSubmenu" aria-expanded="false" aria-controls="emailSubmenu">
    <span class="fas fa-envelope"></span> Email
  </a>
  <div id="emailSubmenu" class="collapse pl-4">
    <a href="#" class="list-group-item list-group-item-action">Cuentas</a>
    <a href="#" class="list-group-item list-group-item-action">Lista Blanca</a>
    <a href="#" class="list-group-item list-group-item-action">Lista Negra</a>
    <a href="#" class="list-group-item list-group-item-action">Configuración</a>
  </div>

  <!-- Otras opciones -->
  <a href="#"><span class="fas fa-server"></span> Backups</a>
  <a href="servidor.php"><span class="fas fa-layer-group"></span> Version de PHP</a>
</div>

<script>
  function toggleNav() {
    var sidenav = document.getElementById("mySidenav");
    var mainContent = document.getElementById("main");
    // Alterna entre abrir y cerrar el sidebar
    if (sidenav.style.width === "250px") {
      sidenav.style.width = "0";
      mainContent.style.marginLeft = "0";
    } else {
      sidenav.style.width = "250px";
      mainContent.style.marginLeft = "250px";
    }
  }
</script>
