#!/bin/bash

# Log
log() {
    echo -e "[INFO] $1"
}

# Error exit
error_exit() {
    echo -e "[ERROR] $1"
    exit 1
}

# Rutas
SOURCE_DOCKERFILE="/root/docker/panel/html/Dockerfile"
DEST_DOCKERFILE="/root/docker/panel/Dockerfile"
PROJECT_DIR="/root/docker/panel"

# Verificar si el archivo Dockerfile existe en la fuente
if [[ ! -f "$SOURCE_DOCKERFILE" ]]; then
    error_exit "El archivo Dockerfile no existe en $SOURCE_DOCKERFILE"
fi

log "Copiando Dockerfile desde $SOURCE_DOCKERFILE a $DEST_DOCKERFILE"
cp "$SOURCE_DOCKERFILE" "$DEST_DOCKERFILE" || error_exit "No se pudo copiar el Dockerfile."

log "Cambiando al directorio $PROJECT_DIR"
cd "$PROJECT_DIR" || error_exit "No se pudo cambiar al directorio $PROJECT_DIR"

log "Ejecutando docker-compose up -d --build"
docker-compose up -d --build panel_cliente2 || error_exit "Error al ejecutar docker-compose."

log "Proceso completado con éxito."
